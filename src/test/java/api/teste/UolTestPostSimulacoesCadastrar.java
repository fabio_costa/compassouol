package api.teste;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import io.restassured.RestAssured;

public class UolTestPostSimulacoesCadastrar {
	
	
	
	@Test
	public void deveSimularCadastro() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.body("{\r\n"
					+ "  \"nome\": \"Helvecio B Costa\",\r\n"
					+ "  \"cpf\": 50244065837,\r\n"
					+ "  \"email\": \"hcosta@gmail.com\",\r\n"
					+ "  \"valor\": 1200,\r\n"
					+ "  \"parcelas\": 3,\r\n"
					+ "  \"seguro\": true\r\n"
					+ "}")
			.contentType("application/json")
		.when()
			.post("simulacoes")
		.then()
			.log().all()
			.statusCode(201)
			
			
		
		
		
		
		;
		
	}

	@Test
	public void deveSimularCadastroJaExistente() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.body("{\r\n"
					+ "  \"nome\": \"Helvecio B Costa\",\r\n"
					+ "  \"cpf\": 50244065837,\r\n"
					+ "  \"email\": \"hcosta@gmail.com\",\r\n"
					+ "  \"valor\": 1200,\r\n"
					+ "  \"parcelas\": 3,\r\n"
					+ "  \"seguro\": true\r\n"
					+ "}")
			.contentType("application/json")
		.when()
			.post("simulacoes")
		.then()
			.log().all()
			.statusCode(409)
			
			
		
		
		
		
		;
		
	}
	
	@Test
	public void deveSimularlistaDeErros() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.body("{\r\n"
					+ "  \"nome\": \"Maria Silva\",\r\n"
					+ "  \"cpf\": 536202668246,\r\n"
					+ "  \"email\": \"msilva@gmail.com\",\r\n"
					+ "  \"parcelas\": 1,\r\n"
					+ "  \"seguro\": true\r\n"
					+ "}")
			.contentType("application/json")
		.when()
			.post("simulacoes")
		.then()
			.log().all()
			.statusCode(400)
			
			
		
		
		
		
		;
		
	}


}
