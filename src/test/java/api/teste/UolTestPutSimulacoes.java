package api.teste;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;

public class UolTestPutSimulacoes {
	
	@Test
	public void deveSimularAlteracao() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.body("{\r\n"
					+ "  \"nome\": \"Helvecio B Costa\",\r\n"
					+ "  \"cpf\": 50244065837,\r\n"
					+ "  \"email\": \"hcosta@gmail.com\",\r\n"
					+ "  \"valor\": 1200,\r\n"
					+ "  \"parcelas\": 12,\r\n"
					+ "  \"seguro\": true\r\n"
					+ "}")
			.contentType("application/json")
		.when()
			.put("simulacoes/50244065837")
		.then()
			.log().all()
			
			
		;
		
	}
	
	
	@Test
	public void deveSimularAlteracaoCpfNaoEncontrado() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.body("{\r\n"
					+ "    \"nome\": \"Ciclano da Silva Costa\",\r\n"
					+ "    \"cpf\": \"97093236999\",\r\n"
					+ "    \"email\": \"teste@teste.com\",\r\n"
					+ "    \"parcelas\": 5,\r\n"
					+ "    \"seguro\": false\r\n"
					+ "}")
			.contentType("application/json")
		.when()
			.put("simulacoes/97093236999")
		.then()
			.log().all()
			.statusCode(404)
			.body("mensagem", is("CPF 97093236999 n�o encontrado"))
			
			
		
		
		
		
		;
		
	}

}
