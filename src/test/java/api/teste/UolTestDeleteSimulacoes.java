package api.teste;

import static io.restassured.RestAssured.*;

import org.junit.Test;

import io.restassured.RestAssured;

public class UolTestDeleteSimulacoes {
	
	
	
	@Test
	public void deveSimularDeleteIdNaoEncontardo() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.contentType("application/json")
		.when()
			.delete("simulacoes/15")
		.then()
			.log().all()
			.statusCode(404)
			
			
			
		;
		
	}
	
	
	@Test
	public void deveSimularDelete() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.contentType("application/json")
		.when()
			.delete("simulacoes/18")
		.then()
			.log().all()
			.statusCode(204)
			
			
		;
		
	}
	

}
