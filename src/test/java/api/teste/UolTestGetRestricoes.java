package api.teste;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;

public class UolTestGetRestricoes {
	
	@Test
	public void deveConsultarRstricoesCpf() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.contentType("application/json")
		.when()
			.get("/restricoes/97093236014")
		.then()
			.log().all()
			.statusCode(200)
			.body("mensagem", is("O CPF 97093236014 possui restri��o"))
			
		
		
		
		
		
		;
		
	}
	
	@Test
	public void deveConsultarNaoPossuiRestricoes() {
		
		RestAssured.baseURI = "http://localhost:8888/api";
		RestAssured.port = 8888;
		RestAssured.basePath = "/v1";
		
		given()
			.log().all()
			.contentType("application/json")
		.when()
			.get("/restricoes/00290408849")	
		.then()
			.log().all()
			.statusCode(204)
			
			
		
		
		
		
		
		;
		
	}

}
